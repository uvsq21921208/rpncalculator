package exceptions;

public class OperationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public OperationException(){
		super("Expected an operation");
	}
}
