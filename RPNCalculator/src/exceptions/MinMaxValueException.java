package exceptions;

public class MinMaxValueException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MinMaxValueException() {
		super("Operandes should be between "+Double.MIN_VALUE+" and "+Double.MAX_VALUE);
	}
}
