package exceptions;

public class DivisionByZero extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DivisionByZero() {
		super("you cannot divide by zero");
	}

}
