package exceptions;

public class MiniMumOperandNeeded extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public MiniMumOperandNeeded(){
		super("You need at least 2 operandes");
	}
}
