package RPNcalculator;

import exceptions.DivisionByZero;
import exceptions.MinMaxValueException;
import exceptions.MiniMumOperandNeeded;
import exceptions.OperationException;

public enum CalculatriceRPN {
	MAINPROGRAM;

	public void execute() {
		SaisieRPN saisieInterection = new SaisieRPN();
		try {
			saisieInterection.interactWithUser();
		} catch (OperationException | MiniMumOperandNeeded | DivisionByZero | MinMaxValueException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {

		CalculatriceRPN.MAINPROGRAM.execute();
	}

}
