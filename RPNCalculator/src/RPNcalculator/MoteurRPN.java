package RPNcalculator;

import java.util.Stack;
import java.util.regex.Pattern;

import exceptions.DivisionByZero;
import exceptions.MiniMumOperandNeeded;
import exceptions.OperationException;

public class MoteurRPN {
	// Stack of operands.
	private Stack<Double> stackOfOperands = new Stack<Double>();

	// Push a value into the stack of operands.
	public void enregistrer(double operand) {
		stackOfOperands.push(operand);
	}

	/**
	 * @param operation the operation that will be applied between the two operands.
	 * @return the result after applying the operation.
	 * @throws MiniMumOperandNeeded if there are less than 2 operands.
	 * @throws DivisionByZero if you try to divide by zero.
	 */
	public double calcul(Operation operation) throws MiniMumOperandNeeded, DivisionByZero {
		if (stackOfOperands.size() >= 2) {
			stackOfOperands.push(operation.eval(stackOfOperands.pop(), stackOfOperands.pop()));
			return stackOfOperands.lastElement();
		} else {
			throw new MiniMumOperandNeeded();
		}
	}

	// Check if a given string is an operator or not
	/**
	 * 
	 * @param symbol = Operation symbol
	 * @return True if the given symbol is a an operation symbol.
	 * @throws OperationException when the given symbol is not a an operation
	 *                            symbol.
	 */
	public boolean isOperator(String symbol) throws OperationException {

		boolean isOperation = Pattern.compile("[+-/*]").matcher(symbol).matches();
		if (isOperation) {
			return isOperation;
		} else {
			throw new OperationException();
		}

	}

	/**
	 * 
	 * @param operationEntered : The operation we are searching for
	 * @return an operation constant (DIV,MULT,SOUS,ADD)
	 */
	public Operation getOperation(String operationEntered) {
		for (Operation operation : Operation.values()) {
			if (operation.getSymbol().equals(operationEntered))
				return operation;
		}
		return null;
	}

	/**
	 * 
	 * @return Operands stack.
	 */
	public Stack<Double> getStack() {
		return this.stackOfOperands;
	}

	public double[] getListOfOperandes() {
		double[] values = new double[stackOfOperands.size()];
		int index = 0;
		for (double value : stackOfOperands) {
			values[index++] = value;
		}
		return values;
	}
}
