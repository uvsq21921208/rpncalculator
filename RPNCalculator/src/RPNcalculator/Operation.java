package RPNcalculator;

import exceptions.DivisionByZero;

public enum Operation {
	PLUS("+") {

		double eval(double x, double y) {
			return y + x;
		}

	},
	MOINS("-") {
		double eval(double x, double y) {
			return y - x;
		}
	},
	MULT("*") {
		double eval(double x, double y) {
			return y * x;
		}
	},
	DIV("/") {
		double eval(double x, double y) throws DivisionByZero {
			if (x == 0)
				throw new DivisionByZero();
			return y / x;
		}
	};

	private String symbol = "";

	abstract double eval(double x, double y) throws DivisionByZero;

	Operation(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return this.symbol;
	}

}
