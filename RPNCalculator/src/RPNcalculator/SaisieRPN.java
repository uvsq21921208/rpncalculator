package RPNcalculator;

import java.util.Scanner;

import exceptions.DivisionByZero;
import exceptions.MinMaxValueException;
import exceptions.MiniMumOperandNeeded;
import exceptions.OperationException;

public class SaisieRPN {
	/**
	 * Check if the value is between (MIN_VALUE and MAX_VALUE).
	 * 
	 * @param value
	 * @return True if value between (MIN_VALUE and MAX_VALUE), else throws an
	 *         exception
	 * @throws MinMaxValueException
	 */
	public boolean limitCheck(double value) throws MinMaxValueException {
		boolean valueCheck = (value < Double.MAX_VALUE && value > Double.MIN_VALUE);
		if (!valueCheck)
			throw new MinMaxValueException();
		return true;
	}

	/**
	 * A helper function to handle interactions with user.
	 * 
	 * @throws OperationException
	 * @throws MiniMumOperandNeeded
	 * @throws DivisionByZero
	 * @throws MinMaxValueException
	 */
	public void interactWithUser()
			throws OperationException, MiniMumOperandNeeded, DivisionByZero, MinMaxValueException {
		double result;
		MoteurRPN moteur = new MoteurRPN();
		Scanner myScanner = new Scanner(System.in);
		String word = "";
		System.out.println("Entrez un op�rende, une operation ou bien tapez exit pour quitter.");
		String currentExpression = "";
		while (true) {

			if (myScanner.hasNextDouble()) {
				double value = myScanner.nextDouble();
				word = String.valueOf(value);
				if (!limitCheck(value)) {
					myScanner.close();
				}
				// Push the scanned value.
				moteur.enregistrer(value);
				double values[] = moteur.getListOfOperandes();
				currentExpression += " " + value;
				System.out.println("L'expression courante " + currentExpression);
				word = myScanner.nextLine();
			}

			else {
				word = myScanner.nextLine();

				if (!word.equals("exit") && moteur.isOperator(word)) {

					currentExpression += " " + word;
					System.out.println("L'expression courante : " + currentExpression);
					result = moteur.calcul(moteur.getOperation(word));
					if (!limitCheck(result)) {
						myScanner.close();
					}

					System.out.println("Resultat : " + result);
				} else {
					if (word.equals("exit")) {
						myScanner.close();
						break;
					}

				}

			}

		}
		System.out.println("Exited");
	}
}
