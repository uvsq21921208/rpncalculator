package RPNcalculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import exceptions.DivisionByZero;
import exceptions.MinMaxValueException;
import exceptions.MiniMumOperandNeeded;

public class SaisieRPNTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void MinimumValueExceptionExceptionTest() throws MinMaxValueException {

		double x = Double.MIN_VALUE - 564456456;
		SaisieRPN saisie = new SaisieRPN();
		expectedException.expect(MinMaxValueException.class);
		saisie.limitCheck(x);

	}

	@Test
	public void MaximumValueExceptionExceptionTest() throws MinMaxValueException {

		double x = Double.MAX_VALUE + 564456456;
		SaisieRPN saisie = new SaisieRPN();
		expectedException.expect(MinMaxValueException.class);
		saisie.limitCheck(x);

	}

	@Test
	public void MaximumValueExceptionExceptionAfterCalculTest()
			throws MinMaxValueException, MiniMumOperandNeeded, DivisionByZero {

		double x = 1.5e308;
		double y = 1.5e308;
		MoteurRPN moteur = new MoteurRPN();
		SaisieRPN saisie = new SaisieRPN();
		moteur.enregistrer(x);
		moteur.enregistrer(y);
		double result = moteur.calcul(Operation.MULT);
		expectedException.expect(MinMaxValueException.class);
		saisie.limitCheck(result);

	}

	@Test
	public void MinimumValueExceptionExceptionAfterCalculTest()
			throws MinMaxValueException, MiniMumOperandNeeded, DivisionByZero {

		double x = 1.5e308;
		double y = -1.5e308;
		MoteurRPN moteur = new MoteurRPN();
		SaisieRPN saisie = new SaisieRPN();
		moteur.enregistrer(x);
		moteur.enregistrer(y);
		double result = moteur.calcul(Operation.MULT);
		expectedException.expect(MinMaxValueException.class);
		saisie.limitCheck(result);

	}

}
