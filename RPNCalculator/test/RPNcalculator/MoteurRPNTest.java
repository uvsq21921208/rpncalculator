package RPNcalculator;

import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import exceptions.DivisionByZero;
import exceptions.MiniMumOperandNeeded;
import exceptions.OperationException;

public class MoteurRPNTest {
	MoteurRPN moteur;
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Before

	public void setUp() {

		moteur = new MoteurRPN();
	}

	@Test
	// Operations tests
	public void isOperatorTest() throws OperationException {
		String operation = "+";
		assertTrue(moteur.isOperator(operation));
		operation = "-";
		assertTrue(moteur.isOperator(operation));
		operation = "/";
		assertTrue(moteur.isOperator(operation));
		operation = "*";
		assertTrue(moteur.isOperator(operation));
		operation = "azeazeaze+";
		exceptionRule.expect(OperationException.class);
		moteur.isOperator(operation);
		operation = "+a";
		moteur.isOperator(operation);

	}

	@Test

	public void calculTestDivisionByZeroExceptionTest() throws MiniMumOperandNeeded, DivisionByZero {
		double x = 5;
		double y = 0;
		moteur.enregistrer(x);
		moteur.enregistrer(y);
		exceptionRule.expect(DivisionByZero.class);
		moteur.calcul(Operation.DIV);

	}

}
